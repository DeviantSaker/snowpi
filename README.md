# README #

### How do I get set up? ###

To make the program run on startup:

1. ```sudo nano /etc/rc.local```
2. Add this to the bottom of the file ```sudo python /home/pi/Documents/Git\ Projects/snowpi/snowpi.py &```

More info on background processes can be found [here](https://anonadventure.wordpress.com/2012/09/13/raspberry-pi-run-in-background/).