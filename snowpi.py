#!/usr/bin/env python

__author__ = "Claire Gurman"

# Standard library imports
from gpiozero import LED
import random
import sys
import time

# Set up all the LEDs
LED_BODY1 = LED(7)  # Body 1
LED_BODY2 = LED(8)  # Body 2
LED_BODY3 = LED(9)  # Body 3
LED_BODY4 = LED(22)  # Body 4
LED_BODY5 = LED(18)  # Body 5
LED_BODY6 = LED(17)  # Body 6
LED_EYE_LEFT = LED(23)  # Left eye
LED_EYE_RIGHT = LED(24)  # Right eye
LED_NOSE = LED(25)  # Nose

TIME_INTERVAL = 300  # Interval before changing pattern (in ticks)
TIME_OFF = 4
TIME_ON = 1


def all_off():
    LED_BODY1.off()
    LED_BODY2.off()
    LED_BODY3.off()
    LED_BODY4.off()
    LED_BODY5.off()
    LED_BODY6.off()
    LED_EYE_LEFT.off()
    LED_EYE_RIGHT.off()
    LED_NOSE.off()


def all_on():
    LED_BODY1.on()
    LED_BODY2.on()
    LED_BODY3.on()
    LED_BODY4.on()
    LED_BODY5.on()
    LED_BODY6.on()
    LED_EYE_LEFT.on()
    LED_EYE_RIGHT.on()
    LED_NOSE.on()


def cycle_all():
    all_off()
    time_start = time.time()  # Time loop started
    while (time.time() - time_start) < TIME_INTERVAL:
        LED_BODY1.toggle()
        time.sleep(1)
        LED_BODY2.toggle()
        time.sleep(1)
        LED_BODY3.toggle()
        time.sleep(1)
        LED_BODY6.toggle()
        time.sleep(1)
        LED_BODY5.toggle()
        time.sleep(1)
        LED_BODY4.toggle()
        time.sleep(1)
        LED_EYE_LEFT.toggle()
        time.sleep(1)
        LED_EYE_RIGHT.toggle()
        time.sleep(1)
        LED_NOSE.toggle()
        time.sleep(1)


def cycle_body():
    all_on()
    time_start = time.time()  # Time loop started
    while (time.time() - time_start) < TIME_INTERVAL:
        LED_BODY1.off()
        LED_BODY2.on()
        LED_BODY3.off()
        LED_BODY4.on()
        LED_BODY5.off()
        LED_BODY6.on()
        time.sleep(0.5)
        LED_BODY1.on()
        LED_BODY2.off()
        LED_BODY3.on()
        LED_BODY4.off()
        LED_BODY5.on()
        LED_BODY6.off()
        time.sleep(0.5)


def wink_left():
    all_on()
    time_start = time.time()  # Time loop started
    while (time.time() - time_start) < TIME_INTERVAL:
        if random.randint(1, 2) == 1:
            LED_EYE_LEFT.toggle()
            time.sleep(1)
            LED_EYE_LEFT.toggle()
            time.sleep(5)


def nose_flash():
    iterations = TIME_INTERVAL / (TIME_ON + TIME_OFF)
    all_on()
    LED_NOSE.blink(on_time=TIME_ON, off_time=TIME_OFF, n=iterations, background=False)

if __name__ == '__main__':
    try:
        all_off()

        while True:
            random_number = random.randint(1, 4)
            if random_number == 1:
                cycle_all()
            elif random_number == 2:
                wink_left()
            elif random_number == 3:
                cycle_body()
            else:
                nose_flash()
    except KeyboardInterrupt:
        print "Interrupted. Stopping."
    except:
        print "Unexpected error:", sys.exc_info()[0]
